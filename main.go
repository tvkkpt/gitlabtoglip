package gitlabtoglip

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"appengine"
	"appengine/urlfetch"
)

func init() {
	http.HandleFunc("/", handler)
}

func handler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	b, _ := ioutil.ReadAll(r.Body)
	r.Body.Close()

	p := NewPush(b)
	if p == nil {
		fmt.Fprint(w, "Not a push")
		return
	}

	g := p.ToGlip()
	if g == nil {
		fmt.Fprint(w, "Nothing to do")
	}

	gb, err := json.Marshal(g)
	if err != nil {
		c.Errorf("Cannot marshal Glip: %s", err.Error())
		return
	}

	glipId := getId(r.URL.Path)
	if glipId == `` {
		fmt.Fprint(w, "Nowhere to send")
		return
	}

	client := urlfetch.Client(c)
	url := "https://hooks.glip.com/webhook/" + glipId
	resp, err := client.Post(url, `application/json`, bytes.NewBuffer(gb))
	if err != nil {
		c.Errorf("Failed to complete Glip webhook: %s", err.Error())
	}
	defer resp.Body.Close()
}

func getId(path string) string {
	idx := strings.LastIndex(path, "/")
	if idx == -1 {
		return ``
	}

	return path[idx+1:]
}
